-- master.dbo.BackupItem definition

-- Drop table

-- DROP TABLE master.dbo.BackupItem;

CREATE TABLE BackupItem (
	Id int IDENTITY(1,1) NOT NULL,
	OriginalData varchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	BackupBeforeEditedByUser int NULL,
	CreateTime datetime NULL,
	CaseType int NOT NULL,
	TableDataIdentity varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ClassName varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ExecutedSql varchar(5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK__BackupIt__3214EC07631DAE49 PRIMARY KEY (Id)
);

 CREATE NONCLUSTERED INDEX TableDataIdentity_index ON dbo.BackupItem (  TableDataIdentity ASC  )  
	 WITH (  PAD_INDEX = OFF ,FILLFACTOR = 100  ,SORT_IN_TEMPDB = OFF , IGNORE_DUP_KEY = OFF , STATISTICS_NORECOMPUTE = OFF , ONLINE = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON  )
	 ON [PRIMARY ] ;


-- master.dbo.[User] definition

-- Drop table

-- DROP TABLE master.dbo.[User];

CREATE TABLE [User] (
	Id int IDENTITY(1,1) NOT NULL,
	Username varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Password varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreateTime datetime NULL,
	ModifyTime datetime NULL,
	IsAdmin bit NULL,
	IsActive bit NULL,
	CONSTRAINT PK__User__3214EC07ED629DEE PRIMARY KEY (Id)
);