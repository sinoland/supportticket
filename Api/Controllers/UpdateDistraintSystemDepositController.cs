﻿using System.Data;
using System.Globalization;
using System.Reflection;
using Api.Model;
using Api.Model.Database;
using Api.Model.Request;
using Api.Models.Database.Backup;
using Api.Repositories;
using Api.Repository;
using Api.Service;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

/**
 * https://confluence.sino-land.com/pages/viewpage.action?pageId=400588840
 */
[ApiController]
[Route("[controller]")]
public class UpdateDistraintSystemDepositController : Controller
{
    private readonly IDbConnection _asdb04;
    private readonly BackupItemService _backupItemService;
    private readonly DistraintDbChequeRepository _distraintDbChequeRepository;
    private readonly DistraintDbDetailRepository _distraintDbDetailRepository;
    private readonly DistraintDbChequeTxnRepository _distraintDbChequeTxnRepository;
    private readonly DistraintDbChargeRepository _distraintDbChargeRepository;

    public UpdateDistraintSystemDepositController(DbResolver dbResolver, BackupItemService backupItemService, DistraintDbChequeRepository distraintDbChequeRepository, DistraintDbDetailRepository distraintDbDetailRepository, DistraintDbChequeTxnRepository distraintDbChequeTxnRepository, DistraintDbChargeRepository distraintDbChargeRepository)
    {
        _asdb04 = dbResolver(EnumSinoDB.Asdb04);
        _backupItemService = backupItemService;
        _distraintDbChequeRepository = distraintDbChequeRepository;
        _distraintDbDetailRepository = distraintDbDetailRepository;
        _distraintDbChequeTxnRepository = distraintDbChequeTxnRepository;
        _distraintDbChargeRepository = distraintDbChargeRepository;
    }

    [HttpPost]
    [Route("AddDepositByCashOrCheque")]
    public async Task<IActionResult> AddDepositByCashOrCheque([FromBody] DistraintSystemAddDepositRequest request)
    {
        var guid = Guid.NewGuid();
        DateTime chequeDate = DateTime.MinValue;
        if (!DateTime.TryParseExact(request.ChequeDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out chequeDate))
        {
            return BadRequest();
        }

        var isDistraintSystemBankCodeCorrect = false;

        foreach (FieldInfo field in typeof(DistraintSystemBankCode).GetFields())
        {
            if (field.FieldType == typeof(string) && field.GetValue(null) as string == request.BankCode)
            {
                isDistraintSystemBankCodeCorrect = true;
                break;
            }
        }
        if (!isDistraintSystemBankCodeCorrect)
        {
            return BadRequest();
        }

        string chargeType = DistraintSystemChargeType.Cheque;
        string paymentType = DistraintSystemPaymentType.Cheque;
        if (request.BankCode == DistraintSystemBankCode.Cash)
        {
            chargeType = DistraintSystemChargeType.Cash;
            paymentType = DistraintSystemPaymentType.Cash;
        }

        if (request.ChequeAmt <= 0)
        {
            return BadRequest();
        }


        var chequeDao = await _distraintDbChequeRepository.GetLatest();
        if (chequeDao == null)
        {
            return BadRequest();
        }


        var distraintDetailDao = await _distraintDbDetailRepository.Get(request.ProjectId, request.AcNo, request.DistraintNo);
        if (distraintDetailDao == null)
        {
            return BadRequest();
        }


        var oldCharge = await _distraintDbChargeRepository.Get(distraintDetailDao.SeqNo, chargeType);
        await _backupItemService.Backup<DistraintDbChargeDao>(oldCharge, guid, "charge", new DistraintDbChargeDao
        {
            ChargeAmt = oldCharge.ChargeAmt + request.ChequeAmt,
            DepositAmt = oldCharge.DepositAmt + request.ChequeAmt
        }, EnumSqlConditionActionType.Update);

        _asdb04.Open();
        using (var transaction = _asdb04.BeginTransaction())
        {
            var detailRes = await _distraintDbChequeRepository.Insert(new Model.Database.DistraintDbChequeDao
            {
                ChequeSeqno = chequeDao.ChequeSeqno + 1,
                BankCode = request.BankCode,
                ChequeNo = request.ChequeNo,
                PayerCode = request.PayerCode,
                PayeeCode = request.PayeeCode,
                ChequeDate = chequeDate,
                ChequeAmt = request.ChequeAmt,
                PaymentType = paymentType,
            });

            var txnRes = await _distraintDbChequeTxnRepository.Insert
                (new Model.Database.DistraintDbChequeTxnDao
                {
                    ChequeSeqno = chequeDao.ChequeSeqno + 1,
                    SeqNo = distraintDetailDao.SeqNo,
                    ChargeType = chargeType,
                    Amount = request.ChequeAmt,
                });

            var updateRes = await _distraintDbChargeRepository.UpdateChargeAndDepositBySeqAndChargeType(request.ChequeAmt, distraintDetailDao.SeqNo, chargeType);


            if (detailRes != null && txnRes != null && updateRes != null)
            {
                var backupItem = await _backupItemService.GetOne(guid);
                await _backupItemService.StoreExecutedSql(backupItem.Id, new List<ExecutedSqlJson> { detailRes, txnRes, updateRes });
                transaction.Commit();
            }
            else
            {
                transaction.Rollback();
            }
        }

        var newCharge = await _distraintDbChargeRepository.Get(distraintDetailDao.SeqNo, chargeType);

        return Ok(newCharge);
    }
}

