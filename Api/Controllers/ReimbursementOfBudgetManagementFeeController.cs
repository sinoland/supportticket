﻿using Api.Model;
using Api.Model.Database;
using Api.Models.Database;
using Api.Models.Database.Backup;
using Api.Models.Request;
using Api.Repositories;
using Api.Repository;
using Api.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Reflection;

namespace Api.Controllers;

//https://confluence.sino-land.com/display/ApplicationTeam/Change+the+Reimbursement+of+Budget+Management+Fee
[Route("[controller]")]
[ApiController]
public class ReimbursementOfBudgetManagementFeeController : Controller
{
    private readonly ArReimburseRepository _arReimburseRepository;
    private readonly ProjectRepository _projectRepository;
    private readonly AddressBookRepository _addressBookRepository;
    private readonly IDbConnection _sinoas01;
    private readonly IBackupItemService _backupItemService;

    public ReimbursementOfBudgetManagementFeeController(ArReimburseRepository reimburseRepository, ProjectRepository projectRepository, AddressBookRepository bookRepository, DbResolver sinoas01, IBackupItemService backupItemService)
    {
        _arReimburseRepository = reimburseRepository;
        _projectRepository = projectRepository;
        _addressBookRepository = bookRepository;
        _sinoas01 = sinoas01(EnumSinoDB.Asdb01);
        _backupItemService = backupItemService;
    }

    [HttpPost]
    public async Task<IActionResult> Index([FromBody] ChangetheReimbursementofBudgetManagementFeeRequest request)
    {
        #region checking
        var isContainBuildingName = false;
        foreach (FieldInfo field in typeof(ReimbursementOfBudgetManagementFeeBuildingName).GetFields())
        {
            if (field.FieldType == typeof(string) && field.GetValue(null) as string == request.CompanyName)
            {
                isContainBuildingName = true;
            }
        }
        if (!isContainBuildingName)
        {
            return BadRequest();

        }

        var isContainChargeCode = false;
        foreach (FieldInfo field in typeof(ArReimburseChargeCode).GetFields())
        {
            if (field.FieldType == typeof(string) && field.GetValue(null) as string == request.ChargeCode)
            {
                isContainChargeCode = true;
            }
        }
        if (!isContainChargeCode)
        {
            return BadRequest();

        }

        var projects = await _projectRepository.GetProjectDao(request.ProjectName);
        if (projects.Count() < 1 || request.ProjectName.Length < 5)
        {
            return BadRequest();
        }

        var project = projects.Where(p => p.LongNameEn == request.ProjectName).FirstOrDefault();
        if (project == null)
        {
            return BadRequest();
        }

        var addressBooks = await _addressBookRepository.GetItems(request.CompanyName, "L");
        if (!addressBooks.Any())
        {
            return NotFound();
        }
        #endregion
        var addressBookDao = addressBooks.First();
        var payeeCode = addressBookDao.AbookCode;
        var projectName = project.LongNameEn;
        var projectCode = project.ProjectCode;
        var landlordCode = project.LandlordCode;
        var chargeCode = request.ChargeCode;
        var budget = request.Budget;
        var guid = Guid.NewGuid();
        var now = DateTime.Now;



        var arReimburses = await _arReimburseRepository.Get(projectCode, new List<string> { chargeCode }, payeeCode);
        _sinoas01.Open();
        using (var transaction = _sinoas01.BeginTransaction())
        {
            var isTransactionSuccess = new bool[arReimburses.Count()];
            if (arReimburses.Any())
            {
                for (int i = 0; i < arReimburses.Count(); i++)
                {
                    var item = arReimburses.ElementAt(i);

                    await _backupItemService.Backup<ArReimburseDao>(item, guid, "ar_reimburse", new ArReimburseDao
                    {
                        BudgetAmt = budget,
                        AmendDate = now,
                    }, EnumSqlConditionActionType.Update);

                    item.AmendDate = now;
                    item.BudgetAmt = budget;
                    var executedObj = await _arReimburseRepository.Update(item);
                    isTransactionSuccess[i] = (executedObj) != null;

                    if (executedObj != null)
                    {
                        var backup = await _backupItemService.GetOne(guid);
                        await _backupItemService.StoreExecutedSql(backup.Id, new List<ExecutedSqlJson> { executedObj });
                    }
                }

                if (isTransactionSuccess.All(b => b))
                {
                    transaction.Commit();
                }
                else
                    transaction.Rollback();
            }
            else
            {
                var dao = new ArReimburseDao
                {
                    ProjectCode = projectCode,
                    LandlordType = "L",
                    LandlordCode = landlordCode,
                    LandlordSubcode = "",
                    PayerType = "L",
                    PayerCode = landlordCode,
                    PayerSubcode = "",
                    ChargeCode = chargeCode,
                    DnoteOnly = 0,
                    ReimburseDesc = "",
                    PayeeType = "L",
                    PayeeCode = payeeCode,
                    PayeeSubcode = "",
                    ReimbursePct = 100,
                    BudgetAmt = budget,
                    CreateUser = "sa",
                    CreateDate = now,
                    AmendUser = "sa",
                    AmendDate = now,
                };
                var res = await _arReimburseRepository.Insert(dao);
                await _backupItemService.Backup<ArReimburseDao>(dao, guid, "ar_reimburse", dao, EnumSqlConditionActionType.Insert);

                if (res != null)
                {
                    var backup = await _backupItemService.GetOne(guid);
                    await _backupItemService.StoreExecutedSql(backup.Id, new List<ExecutedSqlJson> { res });
                    transaction.Commit();
                }
                else
                    transaction.Rollback();

                arReimburses = await _arReimburseRepository.Get(projectCode, new List<string> { chargeCode }, payeeCode);
            }
        }

        return Ok(arReimburses);
    }
}
