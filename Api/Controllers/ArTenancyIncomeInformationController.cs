﻿using System.Data;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Nodes;
using Api.Model;
using Api.Model.Database;
using Api.Model.Request;
using Api.Models.Database.Backup;
using Api.Repository;
using Api.Service;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

/*
 * https://confluence.sino-land.com/pages/viewpage.action?pageId=13735840
 * iPrecise Updating A/R Tenancy Income Information Eff. End Date
 */
[ApiController]
[Route("[controller]")]
public class ArTenancyIncomeInformationController : Controller
{
    private readonly IBackupItemService _backupItemService;
    private readonly ArTenancyIncomeRepository _arTenancyIncomeRepository;
    private readonly ArIncomeBdRepository _arIncomeBdRepository;
    private readonly ArTenancyRepository _arTenancyRepository;
    private readonly IDbConnection _sinoAs01Db;

    public ArTenancyIncomeInformationController(IBackupItemService backupItemService,
        ArTenancyIncomeRepository arTenancyIncomeRepository, ArIncomeBdRepository arIncomeBdRepository, ArTenancyRepository arTenancyRepository,
        DbResolver sinoAs01Db)
    {
        _backupItemService = backupItemService;
        _arTenancyIncomeRepository = arTenancyIncomeRepository;
        _arIncomeBdRepository = arIncomeBdRepository;
        _arTenancyRepository = arTenancyRepository;
        _sinoAs01Db = sinoAs01Db(EnumSinoDB.Asdb01);
    }

    //[Authorize]
    [HttpGet]
    [Route("effect-date")]
    public async Task<IActionResult> GetEffectDateInfo(
        [FromQuery] ArTenancyIncomeInformationEffEndDateGetRequest request)
    {
        var r = await _arTenancyIncomeRepository.Get(request.ProjectCode, request.TenantNo, request.SeqNo);

        return Ok(r);
    }

    //[Authorize]
    [HttpPost]
    [Route("effect-date")]
    public async Task<IActionResult> SetEffectDateInfo(
        [FromBody] ArTenancyIncomeInformationEffEndDateUpdateRequest request)
    {
        var guid = Guid.NewGuid();
        var arTenancyIncomeDaos =
            await _arTenancyIncomeRepository.Get(request.ProjectCode, request.TenantNo, request.SeqNo, request.EffDate);
        var arIncomeBdDaos =
            await _arIncomeBdRepository.Get(request.ProjectCode, request.TenantNo, request.SeqNo, request.EffDate);
        var arTenancyDaos =
            await _arTenancyRepository.Get(request.ProjectCode, request.TenantNo, request.SeqNo);

        if (arTenancyIncomeDaos.Count() == 0 && arIncomeBdDaos.Count() == 0 && arTenancyDaos.Count() == 0)
        {
            return NotFound();
        }

        foreach (var dao in arTenancyIncomeDaos)
        {
            await _backupItemService.Backup<ArTenancyIncomeDao>(dao, guid, $"ar_tenancy_income",
                new ArTenancyIncomeDao() { EndDate = request.EndDate }, EnumSqlConditionActionType.Update);
        }

        foreach (var dao in arIncomeBdDaos)
        {
            await _backupItemService.Backup<ArIncomeBdDao>(dao, guid, $"ar_tenancy_income",
                new ArIncomeBdDao() { EndDate = request.EndDate }, EnumSqlConditionActionType.Update);
        }

        foreach (var dao in arTenancyDaos)
        {
            await _backupItemService.Backup<ArTenancyDao>(dao, guid, $"ar_tenancy_income",
                new ArTenancyDao() { EndDate = request.EndDate }, EnumSqlConditionActionType.Update);
        }

        _sinoAs01Db.Open();
        using (var transaction = _sinoAs01Db.BeginTransaction())
        {
            var affectedIncomeBd = await _arIncomeBdRepository.UpdateEndDate(request.ProjectCode, request.TenantNo,
                request.SeqNo, request.EffDate,
                request.EndDate);
            var affectedTenancyIncome = await _arTenancyIncomeRepository.UpdateEndDate(request.ProjectCode,
                request.TenantNo, request.SeqNo,
                request.EffDate,
                request.EndDate);
            var arTenancy = await _arTenancyRepository.UpdateEndDate(request.ProjectCode, request.TenantNo,request.SeqNo,request.EndDate);

            if (affectedIncomeBd !=null&& affectedTenancyIncome != null& arTenancy!=null)
            {
                var backupItem = await _backupItemService.GetOne(guid);
                await _backupItemService.StoreExecutedSql(backupItem.Id, new List<ExecutedSqlJson> { affectedIncomeBd,affectedTenancyIncome, arTenancy});
                transaction.Commit();
                return Ok();
            }
            else
                transaction.Rollback();
        }

        return BadRequest();
    }
}
