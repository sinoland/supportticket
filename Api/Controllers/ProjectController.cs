﻿using Api.Repositories;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers;
[Route("[controller]")]
[ApiController]
public class ProjectController : ControllerBase
{
    private readonly ProjectRepository _projectRepository;
    private readonly ILogger<ProjectController> _logger;

    public ProjectController(ProjectRepository projectRepository, ILogger<ProjectController> logger)
    {
        _projectRepository = projectRepository;
        _logger = logger;
    }

    // GET: api/<ProjectController>
    [HttpGet]
    [Route("names")]
    public async Task<IActionResult> GetNames([FromQuery] string longNameEn)
    {
        _logger.LogDebug("get names");
        try
        {
            var r = await _projectRepository.GetProjectDao(longNameEn);
            _logger.LogDebug("got names");

            return Ok(r.Select(r=>r.LongNameEn));
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

}
