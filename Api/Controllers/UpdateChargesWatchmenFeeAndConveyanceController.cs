﻿using System.Data;
using Api.Model;
using Api.Model.Database;
using Api.Model.Request;
using Api.Models.Database.Backup;
using Api.Repository;
using Api.Service;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[Route("[controller]")]
[ApiController]
public class UpdateChargesWatchmenFeeAndConveyanceController : Controller
{
    private readonly DistraintdbChargeRegionRepository _distraintdbChargeRegionRepository;
    private readonly IBackupItemService _backupItemService;
    private readonly IDbConnection _asdb04;


    public UpdateChargesWatchmenFeeAndConveyanceController(DistraintdbChargeRegionRepository distraintdbChargeRegionRepository, IBackupItemService backupItemService, DbResolver asdb04)
    {


        _distraintdbChargeRegionRepository = distraintdbChargeRegionRepository;
        _backupItemService = backupItemService;
        _asdb04 = asdb04(EnumSinoDB.Asdb04);
    }

    [HttpPost]
    public async Task<IActionResult> Index([FromBody] IEnumerable<UpdateChargesWatchmenFeeAndConveyanceUpdateRequest> request)
    {
        if (request.Count() != 6)
        {
            return BadRequest();
        }

        var regionIds = new List<string> { "HKG", "KLN", "NT" };
        var chargeTypes = new List<string> { "B", "V" };
        foreach (var item in request)
        {
            if (!regionIds.Any(v1 => v1.Equals(item.RegionId)))
            {
                return BadRequest();
            }

            if (!chargeTypes.Any(v => v == item.ChargeType))
            {
                return BadRequest();
            }
        }

        var guid = Guid.NewGuid();

        var conveyanceRes = await _distraintdbChargeRegionRepository.Get(DistraintdbChargeRegionChargeType.Conveyance);
        var watchmenfeeRes =
            await _distraintdbChargeRegionRepository.Get(DistraintdbChargeRegionChargeType.Watchmenfee);

        foreach (var dao in conveyanceRes)
        {
            await _backupItemService.Backup<DistraintdbChargeRegionDao>(dao, guid, $"[distraintdb].[dbo].[charge_region]",
                dao, EnumSqlConditionActionType.Insert);
        }

        foreach (var dao in watchmenfeeRes)
        {
            await _backupItemService.Backup<DistraintdbChargeRegionDao>(dao, guid, $"[distraintdb].[dbo].[charge_region]",
                dao, EnumSqlConditionActionType.Insert);
        }

        _asdb04.Open();
        var count = 0;
        List<ExecutedSqlJson> l = new List<ExecutedSqlJson>();

        using (var transaction = _asdb04.BeginTransaction())
        {
            try
            {
                foreach (var req in request)
                {
                    var res = await _distraintdbChargeRegionRepository.Insert(new DistraintdbChargeRegionDao()
                    {
                        AmendDate = DateTime.Now,
                        AmendUser = "sa",
                        ChargeAmt = req.ChargeAmt,
                        CreateDate = DateTime.Now,
                        EffDate = req.EffDate,
                        RegionId = req.RegionId,
                        RoundDigit = 0,
                        Percentage = 0,
                        ChargeInd = "A",
                        CreateUser = "sa",
                        ChargeType = req.ChargeType,
                    });

                    if (res != null)
                    {
                        l.Add(res);
                        count += 1;
                    }
                }

                if (count == request.Count())
                {
                    var backupItem = await _backupItemService.GetOne(guid);
                    if(backupItem != null)
                    {
                        await _backupItemService.StoreExecutedSql(backupItem.Id, l);
                    }
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return BadRequest();
            }
        }

        var newConveyanceRes = await _distraintdbChargeRegionRepository.Get(DistraintdbChargeRegionChargeType.Conveyance);
        var newWatchmenfeeRes =
            await _distraintdbChargeRegionRepository.Get(DistraintdbChargeRegionChargeType.Watchmenfee);

        return Ok(newConveyanceRes.Concat(newWatchmenfeeRes));
    }
}
