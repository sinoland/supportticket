﻿using Api.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;
[Route("[controller]")]
[ApiController]
public class AddressBookController : Controller
{
    private readonly AddressBookRepository _addressBookRepository;

    public AddressBookController(AddressBookRepository addressBookRepository)
    {
        _addressBookRepository = addressBookRepository;
    }

    [HttpGet]
    [Route("names")]
    public async Task<IActionResult> GetAddressByName([FromQuery] string name)
    {
        var names = await _addressBookRepository.GetItems(name);
        var set = new HashSet<string>();
        foreach (var item in names)
        {
            set.Add(item.Name1En);
        }

        return Ok(set);
    }
}
