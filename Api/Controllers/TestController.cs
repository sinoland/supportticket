﻿using System.Data;
using Api.Model.Database;
using Dapper;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[Route("[controller]")]
[ApiController]
public class TestController : Controller
{
    private readonly IConfiguration _configuration;
    private readonly IDbConnection _db;
    private readonly ILogger<TestController> _logger;

    public TestController(IConfiguration configuration, IDbConnection db, ILogger<TestController> logger)
    {
        _configuration = configuration;
        _db = db;
        _logger = logger;
    }

    [HttpGet]
    public async Task<IActionResult> Test()
    {
        var a = _configuration;
        _logger.LogInformation("test");
        if (string.IsNullOrEmpty(a["ASDB01"]))
        {
            return BadRequest("No content");
        }
        return Ok(a["ASDB01"]);
    }

    [HttpGet]
    [Route("1")]
    public async Task<IActionResult> Test1([FromQuery] string? t)
    {
        var a = _configuration;
        _logger.LogInformation("test1");
        if (string.IsNullOrEmpty(a["ASDB01"]))
        {
            return BadRequest("No content");
        }
        string s = a["ASDB01"]+"\n"+t;

        return Ok(s);
    }
    
    [HttpGet]

    [Route("Index")]
    public ActionResult Index()
    {
        return Content("Welcome to Support Ticket");
    }
}

