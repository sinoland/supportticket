﻿using Api.Model;
using Api.Service;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class BackupItemController : Controller
{
    private BackupItemService _backupItemService;

    public BackupItemController(BackupItemService backupItemService)
    {
        _backupItemService = backupItemService;
    }

    // [HttpPost]
    // [Route("restore/{caseType}/{tableDataIdentity}")]
    // public async Task<IActionResult> Restore([FromRoute] EnumCaseType caseType, Guid tableDataIdentity)
    // {
    //     await _backupItemService.GetData(tableDataIdentity);
    //     return Ok();
    // }
}
