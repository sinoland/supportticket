﻿using Microsoft.AspNetCore.Mvc;
using Api.Helper;
using Api.Model.Request;
using Api.Models.Request;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class LogController : Controller
{
    [HttpPost]

    [HttpGet]

    [Route("WriteLog")]
    public ActionResult WriteLog([FromBody] LogRequest request)
    {
        LogHelper.WriteLog(request);
        return Ok();
    }
}
