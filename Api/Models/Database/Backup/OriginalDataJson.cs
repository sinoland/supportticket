﻿namespace Api.Model.Database;

public class OriginalDataJson<T>
{
    public T Content { get; set; }
    public string Table { get; set; }
    public T UpdatedFields { get; set; }
    public EnumSqlConditionActionType NewDataActionType { get; set; }  
}