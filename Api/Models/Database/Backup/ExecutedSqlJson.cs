﻿namespace Api.Models.Database.Backup;

public class ExecutedSqlJson
{
    public string PreparedSql { get; set; }
    public object Object { get; set; }
}
