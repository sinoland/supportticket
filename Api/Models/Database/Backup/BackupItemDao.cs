﻿namespace Api.Model.Database;

public class BackupItemDao
{
    public int Id { get; set; }
    public string TableDataIdentity { get; set; }
    public string OriginalData { get; set; }
    public int BackupBeforeEditedByUser { get; set; }
    public DateTime CreateTime { get; set; }
    public EnumCaseType EnumCaseType { get; set; }
    public string ClassName { get; set; }
    public string ExecutedSql { get; set; }
}
