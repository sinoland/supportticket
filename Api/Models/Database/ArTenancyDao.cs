﻿using System.Text.Json.Serialization;
using Dapper;

namespace Api.Model.Database;
public class ArTenancyDao 
{
    [JsonPropertyName("project_code")]
    [Column("project_code")]
    public string ProjectCode { get; set; }

    [JsonPropertyName("tenant_no")]
    [Column("tenant_no")]
    public int TenantNo { get; set; }

    [JsonPropertyName("seq_no")]
    [Column("seq_no")]
    public int SeqNo { get; set; }

    [JsonPropertyName("expiry_date")]
    [Column("expiry_date")]
    public DateTime EndDate { get; set; }
}
