﻿using Api.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Api.Models.Database;

public class DistraintDbDistraintDetailDao
{
    [JsonPropertyName("seq_no")]
    [Column("seq_no")]
    public int SeqNo { get; set; }

    [JsonPropertyName("project_id")]
    [Column("project_id")]
    public string ProjectId { get; set; }

    [JsonPropertyName("ac_no")]
    [Column("ac_no")]
    public string AcNo { get; set; }

    [JsonPropertyName("premises")]
    [Column("premises")]
    public string Premises { get; set; }

    [JsonPropertyName("tenant_name")]
    [Column("tenant_name")]
    public string TenantName { get; set; }

    [JsonPropertyName("landlord_code")]
    [Column("landlord_code")]
    public string LandlordCode { get; set; }

    [JsonPropertyName("distraint_no")]
    [Column("distraint_no")]
    public string DistraintNo { get; set; }

    [JsonPropertyName("distraint_sort_no")]
    [Column("distraint_sort_no")]
    public string DistraintSortNo { get; set; }

    [JsonPropertyName("distressed_amt")]
    [Column("distressed_amt")]
    public string DistressedAmt { get; set; }

    [JsonPropertyName("ini_distressed_amt")]
    [Column("ini_distressed_amt")]
    public string IniDistressedAmt { get; set; }

    [JsonPropertyName("ini_period")]
    [Column("ini_period")]
    public string IniPeriod { get; set; }

    [JsonPropertyName("batch_no")]
    [Column("batch_no")]
    public int BatchNo { get; set; }

    [JsonPropertyName("staff_id")]
    [Column("staff_id")]
    public string StaffId { get; set; }

    [JsonPropertyName("description")]
    [Column("description")]
    public string Description { get; set; }

    [JsonPropertyName("remark")]
    [Column("remark")]
    public string Remark { get; set; }

    [JsonPropertyName("result")]
    [Column("result")]
    public string Result { get; set; }

    [JsonPropertyName("status")]
    [Column("status")]
    public string Status { get; set; }

    [JsonPropertyName("nature")]
    [Column("nature")]
    public string Nature { get; set; }

    [JsonPropertyName("dl_batch_no")]
    [Column("dl_batch_no")]
    public int DlBatchNo { get; set; }

    [JsonPropertyName("dl_seq_no")]
    [Column("dl_seq_no")]
    public int DlSeqNo { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
