﻿using System.Text.Json.Serialization;
using Api.Helper;

namespace Api.Model.Database;

public class DistraintDbChargeDao
{
    [JsonPropertyName("seq_no")]
    [Column("seq_no")]
    public int SeqNo { get; set; }

    [JsonPropertyName("charge_type")]
    [Column("charge_type")]
    public string ChargeType { get; set; }

    [JsonPropertyName("period_desc")]
    [Column("period_desc")]
    public string PeriodDesc { get; set; }

    [JsonPropertyName("charge_amt")]
    [Column("charge_amt")]
    public Decimal ChargeAmt { get; set; }

    [JsonPropertyName("deposit_amt")]
    [Column("deposit_amt")]
    public Decimal DepositAmt { get; set; }

    [JsonPropertyName("refund_amt")]
    [Column("refund_amt")]
    public Decimal RefundAmt { get; set; }

    [JsonPropertyName("oth_refund_amt")]
    [Column("oth_refund_amt")]
    public Decimal OthRefundAmt { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
