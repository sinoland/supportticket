﻿using System.Text.Json.Serialization;
using Api.Helper;
using Api.Models.Database;

namespace Api.Model.Database;

public class DistraintdbChargeRegionDao:IDao
{
    [JsonPropertyName("charge_type")]
    [Column("charge_type")]
    public string ChargeType { get; set; }

    [JsonPropertyName("region_id")]
    [Column("region_id")]
    public string RegionId { get; set; }

    [JsonPropertyName("eff_date")]
    [Column("eff_date")]
    public DateTime EffDate { get; set; }

    [JsonPropertyName("charge_ind")]
    [Column("charge_ind")]
    public string ChargeInd { get; set; }

    [JsonPropertyName("percentage")]
    [Column("percentage")]
    public double Percentage { get; set; }

    [JsonPropertyName("round_digit")]
    [Column("round_digit")]
    public int RoundDigit { get; set; }

    [JsonPropertyName("charge_amt")]
    [Column("charge_amt")]
    public decimal ChargeAmt { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
