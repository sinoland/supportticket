﻿using System.Text.Json.Serialization;
using Api.Helper;

namespace Api.Model.Database;

public class DistraintDbChequeTxnDao
{
    [JsonPropertyName("cheque_seqno")]
    [Column("cheque_seqno")]
    public int ChequeSeqno { get; set; }

    [JsonPropertyName("seq_no")]
    [Column("seq_no")]
    public int SeqNo { get; set; }

    [JsonPropertyName("charge_type")]
    [Column("charge_type")]
    public string ChargeType { get; set; }

    [JsonPropertyName("period_desc")]
    [Column("period_desc")]
    public string PeriodDesc { get; set; }

    [JsonPropertyName("amount")]
    [Column("amount")]
    public decimal Amount { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
