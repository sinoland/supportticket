﻿using System.Text.Json.Serialization;
using Api.Helper;

namespace Api.Model.Database;

public class DistraintDbChequeDao
{
    [JsonPropertyName("cheque_seqno")]
    [Column("cheque_seqno")]
    public int ChequeSeqno { get; set; }

    [JsonPropertyName("bank_code")]
    [Column("bank_code")]
    public string BankCode { get; set; }

    [JsonPropertyName("cheque_no")]
    [Column("cheque_no")]
    public string ChequeNo { get; set; }

    [JsonPropertyName("txn_type")]
    [Column("txn_type")]
    public string TxnType { get; set; }

    [JsonPropertyName("payer_code")]
    [Column("payer_code")]
    public string PayerCode { get; set; }

    [JsonPropertyName("payee_code")]
    [Column("payee_code")]
    public string PayeeCode { get; set; }

    [JsonPropertyName("cheque_date")]
    [Column("cheque_date")]
    public DateTime ChequeDate { get; set; }

    [JsonPropertyName("txn_date")]
    [Column("txn_date")]
    public DateTime TxnDate { get; set; }

    [JsonPropertyName("cheque_amt")]
    [Column("cheque_amt")]
    public Decimal ChequeAmt { get; set; }

    [JsonPropertyName("description")]
    [Column("description")]
    public string Description { get; set; }

    [JsonPropertyName("cheque_status")]
    [Column("cheque_status")]
    public string ChequeStatus { get; set; }

    [JsonPropertyName("payment_type")]
    [Column("payment_type")]
    public string PaymentType { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
