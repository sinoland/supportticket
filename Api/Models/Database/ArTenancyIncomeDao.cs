﻿using System.Text.Json.Serialization;
using Dapper;

namespace Api.Model.Database;

public class ArTenancyIncomeDao
{
    [JsonPropertyName("project_code")]
    [Column("project_code")]
    public string ProjectCode { get; set; }

    [JsonPropertyName("tenant_no")]
    [Column("tenant_no")]
    public int TenantNo { get; set; }

    [JsonPropertyName("seq_no")]
    [Column("seq_no")]
    public int SeqNo { get; set; }

    [JsonPropertyName("eff_date")]
    [Column("eff_date")]
    public DateTime EffDate { get; set; }

    [JsonPropertyName("charge_code")]
    [Column("charge_code")]
    public string ChargeCode { get; set; }

    [JsonPropertyName("end_date")]
    [Column("end_date")]
    public DateTime EndDate { get; set; }

    [JsonPropertyName("bill_freq")]
    [Column("bill_freq")]
    public string BillFreq { get; set; }

    [JsonPropertyName("bill_offset")]
    [Column("bill_offset")]
    public int BillOffset { get; set; }

    [JsonPropertyName("amount")]
    [Column("amount")]
    public string Amount { get; set; }

    [JsonPropertyName("overdue_int")]
    [Column("overdue_int")]
    public int OverdueInt { get; set; }

    [JsonPropertyName("int_method")]
    [Column("int_method")]
    public string IntMethod { get; set; }

    [JsonPropertyName("int_rate")]
    [Column("int_rate")]
    public double IntRate { get; set; }

    [JsonPropertyName("sales_tax")]
    [Column("sales_tax")]
    public int SalesTax { get; set; }

    [JsonPropertyName("tax_rate")]
    [Column("tax_rate")]
    public object TaxRate { get; set; }

    [JsonPropertyName("tax_code")]
    [Column("tax_code")]
    public object TaxCode { get; set; }

    [JsonPropertyName("prorate_tenancy")]
    [Column("prorate_tenancy")]
    public int ProrateTenancy { get; set; }

    [JsonPropertyName("incl_bd")]
    [Column("incl_bd")]
    public int InclBd { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}