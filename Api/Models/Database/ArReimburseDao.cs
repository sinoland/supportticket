﻿using Api.Helper;
using System.Text.Json.Serialization;

namespace Api.Models.Database;

public class ArReimburseDao:IDao
{
    [JsonPropertyName("project_code")]
    [Column("project_code")]
    public string ProjectCode { get; set; }

    [JsonPropertyName("landlord_type")]
    [Column("landlord_type")]
    public string LandlordType { get; set; }

    [JsonPropertyName("landlord_code")]
    [Column("landlord_code")]
    public string LandlordCode { get; set; }

    [JsonPropertyName("landlord_subcode")]
    [Column("landlord_subcode")]
    public string LandlordSubcode { get; set; }

    [JsonPropertyName("payer_type")]
    [Column("payer_type")]
    public string PayerType { get; set; }

    [JsonPropertyName("payer_code")]
    [Column("payer_code")]
    public string PayerCode { get; set; }

    [JsonPropertyName("payer_subcode")]
    [Column("payer_subcode")]
    public string PayerSubcode { get; set; }

    [JsonPropertyName("charge_code")]
    [Column("charge_code")]
    public string ChargeCode { get; set; }

    [JsonPropertyName("dnote_only")]
    [Column("dnote_only")]
    public int DnoteOnly { get; set; }

    [JsonPropertyName("reimburse_desc")]
    [Column("reimburse_desc")]
    public string ReimburseDesc { get; set; }

    [JsonPropertyName("payee_type")]
    [Column("payee_type")]
    public string PayeeType { get; set; }

    [JsonPropertyName("payee_code")]
    [Column("payee_code")]
    public string PayeeCode { get; set; }

    [JsonPropertyName("payee_subcode")]
    [Column("payee_subcode")]
    public string PayeeSubcode { get; set; }

    [JsonPropertyName("reimburse_pct")]
    [Column("reimburse_pct")]
    public double ReimbursePct { get; set; }

    [JsonPropertyName("budget_amt")]
    [Column("budget_amt")]
    public int BudgetAmt { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
