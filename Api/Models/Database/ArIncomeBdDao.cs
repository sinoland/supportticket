﻿using System.Text.Json.Serialization;
using Api.Helper;

namespace Api.Model.Database;

public class ArIncomeBdDao
{
    [JsonPropertyName("project_code")]
    [Column("project_code")]
    public string ProjectCode { get; set; }

    [JsonPropertyName("tenant_no")]
    [Column("tenant_no")]
    public int TenantNo { get; set; }

    [JsonPropertyName("seq_no")]
    [Column("seq_no")]
    public int SeqNo { get; set; }

    [JsonPropertyName("eff_date")]
    [Column("eff_date")]
    public DateTime EffDate { get; set; }

    [JsonPropertyName("charge_code")]
    [Column("charge_code")]
    public string ChargeCode { get; set; }

    [JsonPropertyName("charge_code_bd")]
    [Column("charge_code_bd")]
    public string ChargeCodeBd { get; set; }

    [JsonPropertyName("end_date")]
    [Column("end_date")]
    public DateTime EndDate { get; set; }

    [JsonPropertyName("amount")]
    [Column("amount")]
    public string Amount { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }
}
