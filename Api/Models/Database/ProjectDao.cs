﻿using Api.Helper;
using System.Text.Json.Serialization;

namespace Api.Models.Database;

public class ProjectDao
{
    [JsonPropertyName("project_code")]
    [Column("project_code")]
    public string ProjectCode { get; set; }

    [JsonPropertyName("short_name_en")]
    [Column("short_name_en")]
    public string ShortNameEn { get; set; }

    [JsonPropertyName("short_name_ch")]
    [Column("short_name_ch")]
    public string ShortNameCh { get; set; }

    [JsonPropertyName("short_name_cn")]
    [Column("short_name_cn")]
    public string ShortNameCn { get; set; }

    [JsonPropertyName("long_name_en")]
    [Column("long_name_en")]
    public string LongNameEn { get; set; }

    [JsonPropertyName("long_name_ch")]
    [Column("long_name_ch")]
    public string LongNameCh { get; set; }

    [JsonPropertyName("long_name_cn")]
    [Column("long_name_cn")]
    public string LongNameCn { get; set; }

    [JsonPropertyName("lot_no")]
    [Column("lot_no")]
    public string LotNo { get; set; }

    [JsonPropertyName("ownership")]
    [Column("ownership")]
    public string Ownership { get; set; }

    [JsonPropertyName("landlord_type")]
    [Column("landlord_type")]
    public string LandlordType { get; set; }

    [JsonPropertyName("landlord_code")]
    [Column("landlord_code")]
    public string LandlordCode { get; set; }

    [JsonPropertyName("landlord_subcode")]
    [Column("landlord_subcode")]
    public string LandlordSubcode { get; set; }

    [JsonPropertyName("project_type")]
    [Column("project_type")]
    public string ProjectType { get; set; }

    [JsonPropertyName("project_addr1")]
    [Column("project_addr1")]
    public string ProjectAddr1 { get; set; }

    [JsonPropertyName("project_addr2")]
    [Column("project_addr2")]
    public string ProjectAddr2 { get; set; }

    [JsonPropertyName("project_addr3")]
    [Column("project_addr3")]
    public string ProjectAddr3 { get; set; }

    [JsonPropertyName("district")]
    [Column("district")]
    public string District { get; set; }

    [JsonPropertyName("district_office")]
    [Column("district_office")]
    public string DistrictOffice { get; set; }

    [JsonPropertyName("project_psf")]
    [Column("project_psf")]
    public string ProjectPsf { get; set; }

    [JsonPropertyName("wda_percentage")]
    [Column("wda_percentage")]
    public double WdaPercentage { get; set; }

    [JsonPropertyName("country_origin")]
    [Column("country_origin")]
    public string CountryOrigin { get; set; }

    [JsonPropertyName("currency")]
    [Column("currency")]
    public string Currency { get; set; }

    [JsonPropertyName("currency_sign")]
    [Column("currency_sign")]
    public string CurrencySign { get; set; }

    [JsonPropertyName("currency_text")]
    [Column("currency_text")]
    public string CurrencyText { get; set; }

    [JsonPropertyName("multi_landlord")]
    [Column("multi_landlord")]
    public int MultiLandlord { get; set; }

    [JsonPropertyName("sublease")]
    [Column("sublease")]
    public int Sublease { get; set; }

    [JsonPropertyName("gov_rates")]
    [Column("gov_rates")]
    public int GovRates { get; set; }

    [JsonPropertyName("gov_rent")]
    [Column("gov_rent")]
    public int GovRent { get; set; }

    [JsonPropertyName("manage")]
    [Column("manage")]
    public string Manage { get; set; }

    [JsonPropertyName("floor_area")]
    [Column("floor_area")]
    public string FloorArea { get; set; }

    [JsonPropertyName("period_fr")]
    [Column("period_fr")]
    public DateTime PeriodFr { get; set; }

    [JsonPropertyName("period_to")]
    [Column("period_to")]
    public DateTime PeriodTo { get; set; }

    [JsonPropertyName("billing_day")]
    [Column("billing_day")]
    public int BillingDay { get; set; }

    [JsonPropertyName("grace_day_d")]
    [Column("grace_day_d")]
    public int GraceDayD { get; set; }

    [JsonPropertyName("grace_day_f")]
    [Column("grace_day_f")]
    public int GraceDayF { get; set; }

    [JsonPropertyName("pa_text")]
    [Column("pa_text")]
    public string PaText { get; set; }

    [JsonPropertyName("analysis1")]
    [Column("analysis1")]
    public string Analysis1 { get; set; }

    [JsonPropertyName("analysis2")]
    [Column("analysis2")]
    public string Analysis2 { get; set; }

    [JsonPropertyName("analysis3")]
    [Column("analysis3")]
    public string Analysis3 { get; set; }

    [JsonPropertyName("ibm_projcode")]
    [Column("ibm_projcode")]
    public string IbmProjcode { get; set; }

    [JsonPropertyName("ibm_library")]
    [Column("ibm_library")]
    public string IbmLibrary { get; set; }

    [JsonPropertyName("doc_folder")]
    [Column("doc_folder")]
    public string DocFolder { get; set; }

    [JsonPropertyName("batch_no")]
    [Column("batch_no")]
    public string BatchNo { get; set; }

    [JsonPropertyName("phase_id")]
    [Column("phase_id")]
    public string PhaseId { get; set; }

    [JsonPropertyName("estate_id")]
    [Column("estate_id")]
    public string EstateId { get; set; }

    [JsonPropertyName("project_status")]
    [Column("project_status")]
    public string ProjectStatus { get; set; }

    [JsonPropertyName("project_status_fr")]
    [Column("project_status_fr")]
    public DateTime ProjectStatusFr { get; set; }

    [JsonPropertyName("gstno")]
    [Column("gstno")]
    public string Gstno { get; set; }

    [JsonPropertyName("project_op_date")]
    [Column("project_op_date")]
    public DateTime ProjectOpDate { get; set; }

    [JsonPropertyName("solicitor_type")]
    [Column("solicitor_type")]
    public string SolicitorType { get; set; }

    [JsonPropertyName("solicitor_code")]
    [Column("solicitor_code")]
    public string SolicitorCode { get; set; }

    [JsonPropertyName("solicitor_subcode")]
    [Column("solicitor_subcode")]
    public string SolicitorSubcode { get; set; }

    [JsonPropertyName("solicitor_code_i")]
    [Column("solicitor_code_i")]
    public object SolicitorCodeI { get; set; }

    [JsonPropertyName("solicitor_subcode_i")]
    [Column("solicitor_subcode_i")]
    public object SolicitorSubcodeI { get; set; }

    [JsonPropertyName("solicitor_code_t")]
    [Column("solicitor_code_t")]
    public object SolicitorCodeT { get; set; }

    [JsonPropertyName("solicitor_subcode_t")]
    [Column("solicitor_subcode_t")]
    public object SolicitorSubcodeT { get; set; }

    [JsonPropertyName("solicitor_code_r")]
    [Column("solicitor_code_r")]
    public object SolicitorCodeR { get; set; }

    [JsonPropertyName("solicitor_subcode_r")]
    [Column("solicitor_subcode_r")]
    public object SolicitorSubcodeR { get; set; }

    [JsonPropertyName("solicitor_code_o")]
    [Column("solicitor_code_o")]
    public object SolicitorCodeO { get; set; }

    [JsonPropertyName("solicitor_subcode_o")]
    [Column("solicitor_subcode_o")]
    public object SolicitorSubcodeO { get; set; }

    [JsonPropertyName("solicitor_code_l")]
    [Column("solicitor_code_l")]
    public object SolicitorCodeL { get; set; }

    [JsonPropertyName("solicitor_subcode_l")]
    [Column("solicitor_subcode_l")]
    public object SolicitorSubcodeL { get; set; }

    [JsonPropertyName("ec_billing")]
    [Column("ec_billing")]
    public int EcBilling { get; set; }

    [JsonPropertyName("mgt_project_code")]
    [Column("mgt_project_code")]
    public string MgtProjectCode { get; set; }

    [JsonPropertyName("billmth_offset")]
    [Column("billmth_offset")]
    public int BillmthOffset { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }

    [JsonPropertyName("ozp")]
    [Column("ozp")]
    public string Ozp { get; set; }

    [JsonPropertyName("govt_lease_use")]
    [Column("govt_lease_use")]
    public string GovtLeaseUse { get; set; }

    [JsonPropertyName("dmc_use")]
    [Column("dmc_use")]
    public string DmcUse { get; set; }

    [JsonPropertyName("joint_venture_partner_type")]
    [Column("joint_venture_partner_type")]
    public object JointVenturePartnerType { get; set; }

    [JsonPropertyName("joint_venture_partner_code")]
    [Column("joint_venture_partner_code")]
    public object JointVenturePartnerCode { get; set; }

    [JsonPropertyName("joint_venture_partner_subcode")]
    [Column("joint_venture_partner_subcode")]
    public object JointVenturePartnerSubcode { get; set; }

    [JsonPropertyName("efficiency")]
    [Column("efficiency")]
    public object Efficiency { get; set; }

    [JsonPropertyName("joined_venture")]
    [Column("joined_venture")]
    public object JoinedVenture { get; set; }

    [JsonPropertyName("display_name")]
    [Column("display_name")]
    public string DisplayName { get; set; }

    [JsonPropertyName("landlord_group")]
    [Column("landlord_group")]
    public string LandlordGroup { get; set; }

    [JsonPropertyName("cleaning_clause_office_checking")]
    [Column("cleaning_clause_office_checking")]
    public string CleaningClauseOfficeChecking { get; set; }

    [JsonPropertyName("bill_ac_prefix")]
    [Column("bill_ac_prefix")]
    public string BillAcPrefix { get; set; }

    [JsonPropertyName("bldg_fund_ac1")]
    [Column("bldg_fund_ac1")]
    public string BldgFundAc1 { get; set; }

    [JsonPropertyName("bldg_fund_ac2")]
    [Column("bldg_fund_ac2")]
    public string BldgFundAc2 { get; set; }

    [JsonPropertyName("bldg_fund_ac1_attr")]
    [Column("bldg_fund_ac1_attr")]
    public double BldgFundAc1Attr { get; set; }

    [JsonPropertyName("bldg_fund_ac2_attr")]
    [Column("bldg_fund_ac2_attr")]
    public double BldgFundAc2Attr { get; set; }

    [JsonPropertyName("project_abbr")]
    [Column("project_abbr")]
    public string ProjectAbbr { get; set; }

    [JsonPropertyName("agency")]
    [Column("agency")]
    public string Agency { get; set; }

    [JsonPropertyName("tpl")]
    [Column("tpl")]
    public string Tpl { get; set; }

    [JsonPropertyName("include_tp")]
    [Column("include_tp")]
    public int IncludeTp { get; set; }

    [JsonPropertyName("lg_solicitor_handler_name")]
    [Column("lg_solicitor_handler_name")]
    public object LgSolicitorHandlerName { get; set; }

    [JsonPropertyName("lg_solicitor_handler_tel")]
    [Column("lg_solicitor_handler_tel")]
    public object LgSolicitorHandlerTel { get; set; }

    [JsonPropertyName("enable_fps")]
    [Column("enable_fps")]
    public object EnableFps { get; set; }
}
