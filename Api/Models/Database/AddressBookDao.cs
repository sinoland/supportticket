﻿using Api.Helper;
using System.Text.Json.Serialization;

namespace Api.Models.Database;

public class AddressBookDao
{
    [JsonPropertyName("abook_code")]
    [Column("abook_code")]
    public string AbookCode { get; set; }

    [JsonPropertyName("abook_type")]
    [Column("abook_type")]
    public string AbookType { get; set; }

    [JsonPropertyName("abook_subcode")]
    [Column("abook_subcode")]
    public string AbookSubcode { get; set; }

    [JsonPropertyName("salutation")]
    [Column("salutation")]
    public string Salutation { get; set; }

    [JsonPropertyName("contact_gender")]
    [Column("contact_gender")]
    public string ContactGender { get; set; }

    [JsonPropertyName("contact_fname")]
    [Column("contact_fname")]
    public string ContactFname { get; set; }

    [JsonPropertyName("contact_lname")]
    [Column("contact_lname")]
    public string ContactLname { get; set; }

    [JsonPropertyName("contact_ch")]
    [Column("contact_ch")]
    public string ContactCh { get; set; }

    [JsonPropertyName("contact_title")]
    [Column("contact_title")]
    public string ContactTitle { get; set; }

    [JsonPropertyName("rsp_region")]
    [Column("rsp_region")]
    public string RspRegion { get; set; }

    [JsonPropertyName("region_remark")]
    [Column("region_remark")]
    public string RegionRemark { get; set; }

    [JsonPropertyName("name_type")]
    [Column("name_type")]
    public string NameType { get; set; }

    [JsonPropertyName("id1")]
    [Column("id1")]
    public string Id1 { get; set; }

    [JsonPropertyName("id1_issue_country")]
    [Column("id1_issue_country")]
    public string Id1IssueCountry { get; set; }

    [JsonPropertyName("id2")]
    [Column("id2")]
    public string Id2 { get; set; }

    [JsonPropertyName("id2_issue_country")]
    [Column("id2_issue_country")]
    public string Id2IssueCountry { get; set; }

    [JsonPropertyName("name1_en")]
    [Column("name1_en")]
    public string Name1En { get; set; }

    [JsonPropertyName("name2_en")]
    [Column("name2_en")]
    public string Name2En { get; set; }

    [JsonPropertyName("name1_ch")]
    [Column("name1_ch")]
    public string Name1Ch { get; set; }

    [JsonPropertyName("name2_ch")]
    [Column("name2_ch")]
    public string Name2Ch { get; set; }

    [JsonPropertyName("short_name_en")]
    [Column("short_name_en")]
    public string ShortNameEn { get; set; }

    [JsonPropertyName("short_name_ch")]
    [Column("short_name_ch")]
    public string ShortNameCh { get; set; }

    [JsonPropertyName("trade_name1_en")]
    [Column("trade_name1_en")]
    public string TradeName1En { get; set; }

    [JsonPropertyName("trade_name2_en")]
    [Column("trade_name2_en")]
    public string TradeName2En { get; set; }

    [JsonPropertyName("trade_name1_ch")]
    [Column("trade_name1_ch")]
    public string TradeName1Ch { get; set; }

    [JsonPropertyName("trade_name2_ch")]
    [Column("trade_name2_ch")]
    public string TradeName2Ch { get; set; }

    [JsonPropertyName("reg_office1_en")]
    [Column("reg_office1_en")]
    public string RegOffice1En { get; set; }

    [JsonPropertyName("reg_office2_en")]
    [Column("reg_office2_en")]
    public string RegOffice2En { get; set; }

    [JsonPropertyName("reg_office3_en")]
    [Column("reg_office3_en")]
    public string RegOffice3En { get; set; }

    [JsonPropertyName("reg_office4_en")]
    [Column("reg_office4_en")]
    public string RegOffice4En { get; set; }

    [JsonPropertyName("reg_office1_ch")]
    [Column("reg_office1_ch")]
    public string RegOffice1Ch { get; set; }

    [JsonPropertyName("reg_office2_ch")]
    [Column("reg_office2_ch")]
    public string RegOffice2Ch { get; set; }

    [JsonPropertyName("reg_office3_ch")]
    [Column("reg_office3_ch")]
    public string RegOffice3Ch { get; set; }

    [JsonPropertyName("reg_office4_ch")]
    [Column("reg_office4_ch")]
    public string RegOffice4Ch { get; set; }

    [JsonPropertyName("corr_addr1_en")]
    [Column("corr_addr1_en")]
    public string CorrAddr1En { get; set; }

    [JsonPropertyName("corr_addr2_en")]
    [Column("corr_addr2_en")]
    public string CorrAddr2En { get; set; }

    [JsonPropertyName("corr_addr3_en")]
    [Column("corr_addr3_en")]
    public string CorrAddr3En { get; set; }

    [JsonPropertyName("corr_addr4_en")]
    [Column("corr_addr4_en")]
    public string CorrAddr4En { get; set; }

    [JsonPropertyName("corr_addr1_ch")]
    [Column("corr_addr1_ch")]
    public string CorrAddr1Ch { get; set; }

    [JsonPropertyName("corr_addr2_ch")]
    [Column("corr_addr2_ch")]
    public string CorrAddr2Ch { get; set; }

    [JsonPropertyName("corr_addr3_ch")]
    [Column("corr_addr3_ch")]
    public string CorrAddr3Ch { get; set; }

    [JsonPropertyName("corr_addr4_ch")]
    [Column("corr_addr4_ch")]
    public string CorrAddr4Ch { get; set; }

    [JsonPropertyName("district")]
    [Column("district")]
    public string District { get; set; }

    [JsonPropertyName("country")]
    [Column("country")]
    public string Country { get; set; }

    [JsonPropertyName("country_origin")]
    [Column("country_origin")]
    public string CountryOrigin { get; set; }

    [JsonPropertyName("tel1")]
    [Column("tel1")]
    public string Tel1 { get; set; }

    [JsonPropertyName("tel2")]
    [Column("tel2")]
    public string Tel2 { get; set; }

    [JsonPropertyName("tel_direct")]
    [Column("tel_direct")]
    public string TelDirect { get; set; }

    [JsonPropertyName("tel_mobile")]
    [Column("tel_mobile")]
    public string TelMobile { get; set; }

    [JsonPropertyName("fax")]
    [Column("fax")]
    public string Fax { get; set; }

    [JsonPropertyName("email")]
    [Column("email")]
    public string Email { get; set; }

    [JsonPropertyName("direct_mktg_type")]
    [Column("direct_mktg_type")]
    public string DirectMktgType { get; set; }

    [JsonPropertyName("direct_mktg")]
    [Column("direct_mktg")]
    public string DirectMktg { get; set; }

    [JsonPropertyName("email_mktg")]
    [Column("email_mktg")]
    public int EmailMktg { get; set; }

    [JsonPropertyName("weburl")]
    [Column("weburl")]
    public string Weburl { get; set; }

    [JsonPropertyName("gst_no")]
    [Column("gst_no")]
    public string GstNo { get; set; }

    [JsonPropertyName("trade_type")]
    [Column("trade_type")]
    public string TradeType { get; set; }

    [JsonPropertyName("trade_type_remark")]
    [Column("trade_type_remark")]
    public string TradeTypeRemark { get; set; }

    [JsonPropertyName("analysis1")]
    [Column("analysis1")]
    public string Analysis1 { get; set; }

    [JsonPropertyName("analysis2")]
    [Column("analysis2")]
    public string Analysis2 { get; set; }

    [JsonPropertyName("analysis3")]
    [Column("analysis3")]
    public string Analysis3 { get; set; }

    [JsonPropertyName("analysis4")]
    [Column("analysis4")]
    public string Analysis4 { get; set; }

    [JsonPropertyName("analysis5")]
    [Column("analysis5")]
    public string Analysis5 { get; set; }

    [JsonPropertyName("keep_flag")]
    [Column("keep_flag")]
    public int KeepFlag { get; set; }

    [JsonPropertyName("intra_group")]
    [Column("intra_group")]
    public int IntraGroup { get; set; }

    [JsonPropertyName("cross_selling")]
    [Column("cross_selling")]
    public int CrossSelling { get; set; }

    [JsonPropertyName("cross_selling_o")]
    [Column("cross_selling_o")]
    public int CrossSellingO { get; set; }

    [JsonPropertyName("cross_selling_t")]
    [Column("cross_selling_t")]
    public int CrossSellingT { get; set; }

    [JsonPropertyName("cross_selling_r")]
    [Column("cross_selling_r")]
    public int CrossSellingR { get; set; }

    [JsonPropertyName("cross_selling_i")]
    [Column("cross_selling_i")]
    public int CrossSellingI { get; set; }

    [JsonPropertyName("credit_rating")]
    [Column("credit_rating")]
    public string CreditRating { get; set; }

    [JsonPropertyName("deleted")]
    [Column("deleted")]
    public int Deleted { get; set; }

    [JsonPropertyName("create_user")]
    [Column("create_user")]
    public string CreateUser { get; set; }

    [JsonPropertyName("create_date")]
    [Column("create_date")]
    public DateTime CreateDate { get; set; }

    [JsonPropertyName("amend_user")]
    [Column("amend_user")]
    public string AmendUser { get; set; }

    [JsonPropertyName("amend_date")]
    [Column("amend_date")]
    public DateTime AmendDate { get; set; }

    [JsonPropertyName("ci_no")]
    [Column("ci_no")]
    public string CiNo { get; set; }

    [JsonPropertyName("br_no")]
    [Column("br_no")]
    public string BrNo { get; set; }

    [JsonPropertyName("company_type")]
    [Column("company_type")]
    public string CompanyType { get; set; }

    [JsonPropertyName("opt_out")]
    [Column("opt_out")]
    public string OptOut { get; set; }

    [JsonPropertyName("company_group_id")]
    [Column("company_group_id")]
    public string CompanyGroupId { get; set; }

    [JsonPropertyName("listed_company")]
    [Column("listed_company")]
    public int ListedCompany { get; set; }

    [JsonPropertyName("company_background")]
    [Column("company_background")]
    public string CompanyBackground { get; set; }

    [JsonPropertyName("remarks")]
    [Column("remarks")]
    public string Remarks { get; set; }

    [JsonPropertyName("reg_office5_country")]
    [Column("reg_office5_country")]
    public string RegOffice5Country { get; set; }
}
