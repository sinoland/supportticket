﻿namespace Api.Model.Database;

public class UserDAO
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public DateTime CreateTime { get; set; }
    public DateTime ModifyTime { get; set; }
    public bool IsAdmin { get; set; }
    public bool IsActive { get; set; }
}
