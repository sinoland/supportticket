﻿namespace Api.Model;

public enum Enum
{
}

public enum EnumCaseType
{
    ArTenancyIncomeInformationEffEndDate = 1,
    UpdateChargesWatchmenFeeOrConveyance = 2,
}

public enum EnumSinoDB
{
    Asdb01 = 1,
    Asdb04 = 2,
    DISTRAINTDB = 3,
}

public enum EnumSqlConditionActionType  // which method to restore
{
    Update = 1,
    Delete = 2,
    Insert = 3,
}
/// <summary>
/// log level for log4net
/// </summary>
public enum Log4NetLevel
{
    DEBUG = -1,
    INFO = 0,
    WARN = 1,
    ERROR = 2,
    FATAL = 3,
}

public class DistraintdbChargeRegionChargeType
{
    public static readonly string Watchmenfee = "B";
    public static readonly string Conveyance = "V";
}

public class DistraintSystemBankCode
{
    public static readonly string SinoRealEstateAgencyLtd = "000106";
    public static readonly string HongKongGovernment = "100001";
    public static readonly string RegistrarDistrictCourt = "100002";
    public static readonly string DistrictCourtSuitorsFund = "100003";
    public static readonly string HangSengBank = "100004";
    public static readonly string HongKongBank = "100005";
    public static readonly string BankOfChina = "100006";
    public static readonly string StandardCharteredBank = "100007";
    public static readonly string Cash = "100008";
}

public class DistraintSystemPaymentType
{
    public static readonly string Cheque = "CHQ";
    public static readonly string Cash = "CSH";
}


public class DistraintSystemChargeType
{
    public static readonly string Cheque = "B";
    public static readonly string Cash = "M";
}

// Change the Reimbursement of Budget Management Fee
public class ReimbursementOfBudgetManagementFeeBuildingName
{
    public static readonly string SEML = "Sino Estates Management Limited";
    public static readonly string CBML = "CHKC Building Management Limited";
}

public class ArReimburseChargeCode
{
    public const string BudgetManagementFees = "B00";
    public const string CarParkManagementFees = "D21";
}