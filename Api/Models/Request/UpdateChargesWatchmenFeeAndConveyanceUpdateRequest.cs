﻿using System.ComponentModel.DataAnnotations;

namespace Api.Model.Request;

public class UpdateChargesWatchmenFeeAndConveyanceUpdateRequest
{
    [Required]
    public string ChargeType { get; set; }
    [Required]
    public string RegionId { get; set; }
    [Required]
    public DateTime EffDate { get; set; }
    [Required]
    public Decimal ChargeAmt { get; set; }
}