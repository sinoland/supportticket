using System.ComponentModel.DataAnnotations;

namespace Api.Model.Request;

public class RegisterRequest
{
    [Required]
    public string Username { get; set; }

    [Required]
    public string Password { get; set; }
}
