﻿using System.ComponentModel.DataAnnotations;

namespace Api.Model.Request;

public class ArTenancyIncomeInformationEffEndDateUpdateRequest
{
    [Required]
    public string ProjectCode { get; set; }
    [Required]
    public string TenantNo { get; set; }
    [Required]
    public string SeqNo { get; set; }
    [Required]
    public DateTime EffDate { get; set; }
    [Required]
    public DateTime EndDate { get; set; }
}
