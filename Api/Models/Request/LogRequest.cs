﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models.Request;

public class LogRequest
{
    [Required]
    public string Message { get; set; }
    public int? Level { get; set; }
}
