﻿using System.ComponentModel.DataAnnotations;

namespace Api.Model.Request;

public class DistraintSystemAddDepositRequest
{
    [Required] public string ProjectId { get; set; }
    [Required] public string AcNo { get; set; }
    [Required] public string DistraintNo { get; set; }
    [Required] public string ChequeNo { get; set; }
    [Required] public string ChequeDate { get; set; } //@cheque_date='20201124
    [Required] public string BankCode { get; set; }
    [Required] public string PayerCode { get; set; }
    [Required] public string PayeeCode { get; set; }
    [Required] public decimal ChequeAmt { get; set; }
}
