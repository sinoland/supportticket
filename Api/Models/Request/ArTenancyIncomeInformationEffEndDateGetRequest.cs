﻿using System.ComponentModel.DataAnnotations;

namespace Api.Model.Request;

public class ArTenancyIncomeInformationEffEndDateGetRequest
{
    [Required]
    public string? ProjectCode { get; set; }
    [Required]
    public string? TenantNo { get; set; }
    [Required]
    public string? SeqNo { get; set; }
}
