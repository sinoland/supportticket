﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models.Request;

public class ChangetheReimbursementofBudgetManagementFeeRequest
{
    [Required]
    public string CompanyName { get; set; }
    [Required]
    public string ProjectName { get; set; }
    [Required]
    public string ChargeCode { get; set; }
    [Required]
    public int Budget { get; set; }
}