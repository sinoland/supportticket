﻿using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace Api.Model;

public abstract class BaseBackupableItem
{
    public string fullTableName = "";
    public int? id = 0;

    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }

    public abstract string GetUpdateSql();
    public abstract string GetDeleteSql();
    public abstract string GetInsertSql();

    public abstract object GetInstanceForSql();
}
