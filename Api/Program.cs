using System.Data;
using System.Data.SqlClient;
using System.Text;
using Api.Helper;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database;
using Api.Repositories;
using Api.Repository;
using Api.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog;
using NLog.Web;
using log4net;
using log4net.Config;
using log4net.Repository;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true);
var config = builder.Configuration;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");
// NLog: Setup NLog for Dependency injection
builder.Logging.ClearProviders();
//builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
builder.Host.UseNLog();

builder.Services.AddControllers();
// Add services to the container.
builder.Services.AddSwaggerGen((option) =>
{
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        BearerFormat = "JWT",
        Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 1safsfsdfdfd\"",
        In = ParameterLocation.Header,
        Name = "Authorization",
        Scheme = "Bearer",
        Type = SecuritySchemeType.ApiKey,
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] { }
        }
    });
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = config["JwtAuth:Issuer"],
            ValidAudience = config["JwtAuth:Issuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["JwtAuth:Key"]))
        };
    });

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

Dapper.SqlMapper.SetTypeMap(typeof(AddressBookDao), new ColumnAttributeTypeMapper<AddressBookDao>());
Dapper.SqlMapper.SetTypeMap(typeof(ArIncomeBdDao), new ColumnAttributeTypeMapper<ArIncomeBdDao>());
Dapper.SqlMapper.SetTypeMap(typeof(ArReimburseDao), new ColumnAttributeTypeMapper<ArReimburseDao>());
Dapper.SqlMapper.SetTypeMap(typeof(ArTenancyIncomeDao), new ColumnAttributeTypeMapper<ArTenancyIncomeDao>());
Dapper.SqlMapper.SetTypeMap(typeof(DistraintDbChargeDao), new ColumnAttributeTypeMapper<DistraintDbChargeDao>());
Dapper.SqlMapper.SetTypeMap(typeof(DistraintdbChargeRegionDao), new ColumnAttributeTypeMapper<DistraintdbChargeRegionDao>());
Dapper.SqlMapper.SetTypeMap(typeof(DistraintDbChequeDao), new ColumnAttributeTypeMapper<DistraintDbChequeDao>());
Dapper.SqlMapper.SetTypeMap(typeof(DistraintDbChequeTxnDao), new ColumnAttributeTypeMapper<DistraintDbChequeTxnDao>());
Dapper.SqlMapper.SetTypeMap(typeof(DistraintDbDistraintDetailDao), new ColumnAttributeTypeMapper<DistraintDbDistraintDetailDao>());
Dapper.SqlMapper.SetTypeMap(typeof(ProjectDao), new ColumnAttributeTypeMapper<ProjectDao>());

builder.Services.AddSingleton<AddressBookRepository>();
builder.Services.AddSingleton<ArIncomeBdRepository>();
builder.Services.AddSingleton<ArReimburseRepository>();
builder.Services.AddSingleton<ArTenancyIncomeRepository>();
builder.Services.AddSingleton<ArTenancyRepository>();
builder.Services.AddSingleton<BackupItemRepository>();
builder.Services.AddSingleton<DistraintdbChargeRegionRepository>();
builder.Services.AddSingleton<DistraintDbChargeRepository>();
builder.Services.AddSingleton<DistraintDbChequeRepository>();
builder.Services.AddSingleton<DistraintDbChequeTxnRepository>();
builder.Services.AddSingleton<DistraintDbDetailRepository>();
builder.Services.AddSingleton<ProjectRepository>();
builder.Services.AddSingleton<UserRepository>();


builder.Services.AddScoped<IBackupItemService, BackupItemService>();
builder.Services.AddScoped<IUserService, UserService>();

builder.Services.AddSingleton<BackupItemService>();

var sqlConnectionASDB01 = config["ASDB01"];
var sqlConnectionASDB04 = config["ASDB04"];
var sqlConnectionDistraintDb = config["DISTRAINTDB"];
builder.Services.AddTransient<IDbConnection>(provider => new SqlConnection(config["SELF_DB"]));
builder.Services.AddTransient<DbResolver>(provider => db =>
{
    switch (db)
    {
        case EnumSinoDB.Asdb04:
            return new SqlConnection(sqlConnectionASDB04);
        case EnumSinoDB.DISTRAINTDB:
            return new SqlConnection(sqlConnectionDistraintDb);
        default:
        case EnumSinoDB.Asdb01:
            return new SqlConnection(sqlConnectionASDB01);
    }
});

var app = builder.Build();

ILoggerRepository repository = log4net.LogManager.CreateRepository("NETCoreRepository");
XmlConfigurator.Configure(repository, new FileInfo("log4net.config"));

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "JWTAuthDemo v1"));
    //app.UseCors(options => options.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
    //app.UseHttpsRedirection();
//}
app.UseMiddleware(typeof(ExceptionHandlerHelper));
app.UseCors(options => options.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
app.UseHttpsRedirection();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseHttpLogging();

app.Run();