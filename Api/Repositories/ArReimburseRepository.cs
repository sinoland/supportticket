﻿using Api.Models.Database;
using Api.Repository;
using System.Data;
using Dapper;
using Api.Models.Database.Backup;

namespace Api.Repositories;

public class ArReimburseRepository: IBaseRepository
{
    private readonly IDbConnection _sinoAs01Db;

    public ArReimburseRepository(DbResolver db)
    {
        _sinoAs01Db = db(Model.EnumSinoDB.Asdb01);
    }

    public async Task<ExecutedSqlJson?> Update(IDao dao)
    {
        var sql = @"UPDATE ar_reimburse set project_code= @ProjectCode,landlord_type= @LandlordType,landlord_code= @LandlordCode,landlord_subcode= @LandlordSubcode,payer_type= @PayerType,payer_code= @PayerCode,payer_subcode= @PayerSubcode,charge_code= @ChargeCode,dnote_only= @DnoteOnly,reimburse_desc= @ReimburseDesc,payee_type= @PayeeType,payee_code= @PayeeCode,payee_subcode= @PayeeSubcode,reimburse_pct= @ReimbursePct,budget_amt= CONVERT(money,@BudgetAmt),create_user= @CreateUser,create_date= @CreateDate,amend_user= @AmendUser,amend_date=@AmendDate WHERE [project_code] = @ProjectCode AND [charge_code] = @ChargeCode AND [payee_code] = @PayeeCode";
        var obj = dao;
        var res = await _sinoAs01Db.ExecuteAsync(sql, dao);
        return res > 0 ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null; ;
    }

    public async Task<ExecutedSqlJson?> Insert(IDao dao)
    {
        var sql = @"INSERT INTO ar_reimburse (project_code,landlord_type,landlord_code,landlord_subcode,payer_type,payer_code,payer_subcode,charge_code,dnote_only,reimburse_desc,payee_type,payee_code,payee_subcode,reimburse_pct,budget_amt,create_user,create_date,amend_user,amend_date) VALUES 
(@ProjectCode,@LandlordType,@LandlordCode,@LandlordSubcode,@PayerType,@PayerCode,@PayerSubcode,@ChargeCode,@DnoteOnly,@ReimburseDesc,@PayeeType,@PayeeCode,@PayeeSubcode,@ReimbursePct,@BudgetAmt,@CreateUser,@CreateDate,@AmendUser,@AmendDate )";
        var obj = dao;
        var res = await _sinoAs01Db.ExecuteAsync(sql, dao);
        return res > 0 ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null;
    }

    public async Task<IEnumerable<ArReimburseDao>> Get(string projectCode, IEnumerable<string> chargeCodes, string payeeCode)
    {
        return await _sinoAs01Db.QueryAsync<ArReimburseDao>($"SELECT * FROM ar_reimburse WHERE [project_code] = @ProjectCode AND [charge_code] IN (@ChargeCode) AND [payee_code] = @PayeeCode", new
        {
            @ProjectCode = projectCode,
            @ChargeCode = chargeCodes,
            @PayeeCode = payeeCode,
        });
    }
}
