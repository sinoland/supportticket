﻿using System.Data;
using Api.Model.Database;
using Api.Models.Database.Backup;
using Dapper;

namespace Api.Repository;

public class BackupItemRepository
{
    private readonly IDbConnection _db;

    public BackupItemRepository(IDbConnection db)
    {
        _db = db;
    }

    public async Task<bool> Backup(BackupItemDao item)
    {
        var r = await _db.ExecuteAsync(
            "INSERT INTO BackupItem  (OriginalData, BackupBeforeEditedByUser, CreateTime, CaseType, TableDataIdentity, ClassName)  VALUES (@OriginalData, @BackupBeforeEditedByUser, @CreateTime, @CaseType, @TableDataIdentity, @ClassName)",
            new
            {
                @CaseType = item.EnumCaseType,
                @TableDataIdentity = item.TableDataIdentity,
                @OriginalData = item.OriginalData,
                @BackupBeforeEditedByUser = item.BackupBeforeEditedByUser,
                @CreateTime = DateTime.Now,
                @ClassName = item.ClassName,
            });
        return r > 0;
    }
    
    public async Task<IEnumerable<BackupItemDao>> Get(Guid tableDataIdentity)
    {
        var r = await _db.QueryAsync<BackupItemDao>(
            "SELECT * FROM BackupItem WHERE TableDataIdentity = @TableDataIdentity",
            new
            {
                @TableDataIdentity = tableDataIdentity.ToString(),
            });
        return r;
    }

    public async Task<bool> AddSql(int Id, string json)
    {
        var r = await _db.ExecuteAsync("UPDATE BackupItem SET ExecutedSql = @ExecutedSql WHERE Id = @Id", new
        {
            @ExecutedSql = json,
            @Id = Id,
        });
        return (r > 0);
    }
}