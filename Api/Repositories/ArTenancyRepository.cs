﻿using System.Data;
using System.Text;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database;
using Api.Models.Database.Backup;
using Dapper;

namespace Api.Repository;

public class ArTenancyRepository
{
    private readonly IDbConnection _sinoAs01Db;

    public ArTenancyRepository(DbResolver db)
    {
        _sinoAs01Db = db(EnumSinoDB.Asdb01);
    }

    public async Task<IEnumerable<ArTenancyDao>> Get(string projectCode, string tenantNo, string seqNo)
    {
        var sql = new StringBuilder("SELECT * FROM ar_tenancy");
        var l = new List<string>();
        if (!string.IsNullOrEmpty(projectCode))
            l.Add($"project_code = @project_code");
        if (!string.IsNullOrEmpty(tenantNo))
            l.Add($"tenant_no = @tenant_no");
        if (!string.IsNullOrEmpty(seqNo))
            l.Add($"seq_no = @seq_no");
        if (l.Count > 0)
        {
            sql.Append($" WHERE {string.Join(" AND ", l)}");
        }

        var r = await _sinoAs01Db.QueryAsync<ArTenancyDao>(sql.ToString(), new
        {
            @project_code = projectCode,
            @tenant_no = tenantNo,
            @seq_no = seqNo
        });

        return r;
    }

    public async Task<ExecutedSqlJson?> UpdateEndDate(string projectCode, string tenantNo, string seqNo, DateTime endDate)
    {
        var sql = @"update ar_tenancy set expiry_date = @end_date where project_code = @project_code and tenant_no = @tenant_no and seq_no = @seq_no ";
        var obj = new
        {
            @end_date = endDate,
            @project_code = projectCode,
            @tenant_no = tenantNo,
            @seq_no = seqNo,
        };
        var r =await _sinoAs01Db.ExecuteAsync(sql,obj);

        return (r > 0) ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null; ;
    }
}