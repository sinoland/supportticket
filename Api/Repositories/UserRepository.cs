﻿using System.Data;
using System.Text;
using Api.Model.Database;
using Dapper;

namespace Api.Repository;

public class UserRepository
{
    private readonly IDbConnection _db;

    public UserRepository(IDbConnection db)
    {
        _db = db;
    }

    public async Task<UserDAO> AddUser(UserDAO userDao)
    {
        var result = await _db.QuerySingleAsync<UserDAO>(
            $"INSERT INTO [User] (Username, Password, CreateTime, ModifyTime, IsAdmin, IsActive) VALUES (@Username, @Password, @CreateTime, @ModifyTime, @IsAdmin, @IsActive); SELECT * FROM [User] WHERE Id = SCOPE_IDENTITY();",
            userDao);
        return result;
    }

    public async Task<UserDAO> SelectUser(string username, string password, bool? isActive = null)
    {
        var sql = new StringBuilder("SELECT * FROM [USER] WHERE Username = @Username AND Password = @Password");

        if (isActive.HasValue)
        {
            sql = sql.Append(" AND IsActive = @IsActive");
        }
        var result = await _db.QueryFirstOrDefaultAsync<UserDAO>(sql.ToString(),
            new
            {
                @Username = username,
                @Password = password,
                @IsActive = isActive,
            }
        );
        return result;
    }
}
