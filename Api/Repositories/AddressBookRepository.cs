﻿using Api.Repository;
using System.Data;
using Dapper;
using Api.Models.Database;
using System.Text;

namespace Api.Repositories;

public class AddressBookRepository
{
    private readonly IDbConnection _sinoAs01Db;

    public AddressBookRepository(DbResolver sinoAs01Db)
    {
        _sinoAs01Db = sinoAs01Db(Model.EnumSinoDB.Asdb01);
    }

    public async Task<IEnumerable<AddressBookDao>> GetItems(string name, string? abookType = null)
    {
        var sql = new StringBuilder(@"SELECT * FROM address_book ");
        var l = new List<string>();

        l.Add("name1_en like @Name");
        if (abookType != null)
            l.Add("abook_type = @AbookType");

        if (l.Count > 0)
        {
            sql.Append(" WHERE ");
            sql.Append(string.Join(" AND ", l));
        }

        var res = await _sinoAs01Db.QueryAsync<AddressBookDao>(sql.ToString(), new
        {
            @Name = name + "%",
            @AbookType = abookType
        });
        return res;
    }
}
