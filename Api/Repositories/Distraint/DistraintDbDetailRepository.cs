﻿using Api.Model.Database;
using Api.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Api.Models.Database;
using Api.Model;

namespace Api.Repositories;

public class DistraintDbDetailRepository
{
    private readonly IDbConnection _dbDistraint;
    private readonly IDbConnection _db;

    public DistraintDbDetailRepository(DbResolver db04, IDbConnection db)
    {
        _db = db;
        _dbDistraint = db04(EnumSinoDB.DISTRAINTDB);
    }
    public async Task<DistraintDbDistraintDetailDao> Get(string projectId, string acNo, string distraintNo)
    {
        return await _dbDistraint.QueryFirstOrDefaultAsync<DistraintDbDistraintDetailDao>(@"select * from distraint_detail
where project_id = @ProjectId
and ac_no = @AcNo and distraint_no = @DistraintNo", new
        {
            @ProjectId = projectId,
            @AcNo = acNo,
            @DistraintNo = distraintNo
        });
    }
}
