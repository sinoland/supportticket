﻿using System.Data;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database.Backup;
using Dapper;

namespace Api.Repository;

public class DistraintDbChequeTxnRepository
{
    private readonly IDbConnection _dbDistraint;
    private readonly IDbConnection _db;

    public DistraintDbChequeTxnRepository(DbResolver db04, IDbConnection db)
    {
        _db = db;
        _dbDistraint = db04(EnumSinoDB.DISTRAINTDB);
    }

    public async Task<ExecutedSqlJson?> Insert(DistraintDbChequeTxnDao dao)
    {
        var sql = @"insert into cheque_txn
values(@ChequeSeqno, @SeqNo, @ChargeType, '', @Amount, 'sa', getdate(), 'sa', getdate())";
        var obj = new
        {
            @ChequeSeqno = dao.ChequeSeqno,
            @SeqNo = dao.SeqNo,
            @ChargeType = dao.ChargeType,
            @Amount = dao.Amount,

        };
        var res = await _dbDistraint.ExecuteAsync(sql
            , obj);

        return res > 0 ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null;
    }
}
