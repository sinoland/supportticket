﻿using System.Data;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database;
using Api.Models.Database.Backup;
using Api.Repositories;
using Dapper;

namespace Api.Repository;

public class DistraintdbChargeRegionRepository : IBaseRepository
{
    private readonly IDbConnection _db04;
    private readonly IDbConnection _db;

    public DistraintdbChargeRegionRepository(DbResolver db04, IDbConnection db)
    {
        _db = db;
        _db04 = db04(EnumSinoDB.Asdb04);
    }

    public async Task<ExecutedSqlJson?> Insert(IDao dao)
    {
        var sql = "INSERT INTO [charge_region] (charge_type,region_id,eff_date,charge_ind,percentage,round_digit,charge_amt,create_user,create_date,amend_user,amend_date) VALUES (@ChargeType,@RegionId,@EffDate,@ChargeInd,@Percentage,@RoundDigit,@ChargeAmt,@CreateUser,@CreateDate,@AmendUser,@AmendDate)";
        var obj = dao;
        var res = await _db04.ExecuteAsync(sql, dao);

        return res > 0 ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null;
    }

    public async Task<IEnumerable<DistraintdbChargeRegionDao>> Get(string type)
    {
        var res = await _db04.QueryAsync<DistraintdbChargeRegionDao>(@"  SELECT TOP (3) *
  FROM [charge_region]
  WHERE charge_type=@ChargeType ORDER BY create_date DESC", new
        {
            @ChargeType = type
        });

        return res;
    }

    public Task<ExecutedSqlJson?> Update(IDao dao)
    {
        throw new NotImplementedException();
    }
}
