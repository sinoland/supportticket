﻿using System.Data;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database.Backup;
using Dapper;

namespace Api.Repository;

public class DistraintDbChequeRepository
{
    private readonly IDbConnection _dbDistraint;
    private readonly IDbConnection _db;

    public DistraintDbChequeRepository(DbResolver db04, IDbConnection db)
    {
        _db = db;
        _dbDistraint = db04(EnumSinoDB.DISTRAINTDB);
    }

    public async Task<ExecutedSqlJson?> Insert(DistraintDbChequeDao dao)
    {
        var sql = @"insert into cheque
values(@ChequeSeqno, @BankCode, @ChequeNo, 'D', @PayerCode, @PayeeCode, @ChequeDate, @ChequeDate, @ChequeAmt, '', 'N', @PaymentType, 'sa', getdate(), 'sa', getdate())";
        var obj = new
        {
            @ChequeSeqno = dao.ChequeSeqno,
            @BankCode = dao.BankCode,
            @ChequeNo = dao.ChequeNo,
            @PayerCode = dao.PayerCode,
            @PayeeCode = dao.PayeeCode,
            @ChequeDate = dao.ChequeDate,
            @ChequeAmt = dao.ChequeAmt,
            @PaymentType = dao.PaymentType,
        };
        var res = await _dbDistraint.ExecuteAsync(sql
            , obj);

        return res > 0 ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null;
    }

    public async Task<DistraintDbChequeDao> GetLatest()
    {
        return await _dbDistraint.QueryFirstAsync<DistraintDbChequeDao>(@" SELECT * from cheque ORDER BY cheque_seqno DESC");
    }
}
