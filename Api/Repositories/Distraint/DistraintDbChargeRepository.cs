﻿using System.Data;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database.Backup;
using Dapper;

namespace Api.Repository;

public class DistraintDbChargeRepository
{

    private readonly IDbConnection _dbDistraint;
    private readonly IDbConnection _db;

    public DistraintDbChargeRepository(DbResolver db04, IDbConnection db)
    {
        _db = db;
        _dbDistraint = db04(EnumSinoDB.DISTRAINTDB);
    }

    public async Task<ExecutedSqlJson?> UpdateChargeAndDepositBySeqAndChargeType(Decimal chargeAmt, int seqNo,
        string chargeType)
    {
        var sql = @"update charge set charge_amt = charge_amt + @ChequeAmt, deposit_amt = deposit_amt + @ChequeAmt
where seq_no = @SeqNo and charge_type = @ChargeType";
        var obj = new
        {
            @ChequeAmt = chargeAmt,
            @SeqNo = seqNo,
            @ChargeType = chargeType,
        };
        var res = await _dbDistraint.ExecuteAsync(sql
            , obj);
        return res > 0 ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null;
    }

    public async Task<DistraintDbChargeDao> Get(int seqNo,
        string chargeType)
    {
        return await _dbDistraint.QuerySingleOrDefaultAsync<DistraintDbChargeDao>(@"SELECT * FROM charge  WHERE seq_no = @seqNo and charge_type = @chargeType", new
        {
            @seqNo = seqNo,
            @chargeType = chargeType
        });
    }
}
