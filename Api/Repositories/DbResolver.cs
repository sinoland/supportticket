﻿using System.Data;
using System.Data.SqlClient;
using Api.Model;

namespace Api.Repository;

public delegate IDbConnection DbResolver(EnumSinoDB db);
