﻿using System.Data;
using System.Text;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database.Backup;
using Dapper;

namespace Api.Repository;

public class ArIncomeBdRepository
{
    private readonly IDbConnection _sinoAs01Db;

    public ArIncomeBdRepository(DbResolver db)
    {
        _sinoAs01Db = db(EnumSinoDB.Asdb01);
    }

    public async Task<IEnumerable<ArIncomeBdDao>> Get(string projectCode, string tenantNo, string seqNo,
        DateTime? effDate = null)
    {
        var sql = new StringBuilder("SELECT * FROM ar_income_bd");
        var l = new List<string>();
        if (!string.IsNullOrEmpty(projectCode))
            l.Add($"project_code = @project_code");
        if (!string.IsNullOrEmpty(tenantNo))
            l.Add($"tenant_no = @tenant_no");
        if (!string.IsNullOrEmpty(seqNo))
            l.Add($"seq_no = @seq_no");
        if (effDate.HasValue)
            l.Add($"eff_date = @eff_date");
        if (l.Count > 0)
        {
            sql.Append($" WHERE {string.Join(" AND ", l)}");
        }

        var r = await _sinoAs01Db.QueryAsync<ArIncomeBdDao>(sql.ToString(), new
        {
            @project_code = projectCode,
            @tenant_no = tenantNo,
            @seq_no = seqNo,
            @eff_date = effDate,
        });

        return r;
    }

    public async Task<ExecutedSqlJson?> UpdateEndDate(string projectCode, string tenantNo, string seqNo,
        DateTime effDate, DateTime endDate)
    {
        var sql = @"update ar_income_bd set end_date = @end_date 
where project_code = @project_code and tenant_no = @tenant_no and seq_no = @seq_no and eff_date = @eff_date";
        var obj = new
        {
            @end_date = endDate,
            @project_code = projectCode,
            @tenant_no = tenantNo,
            @seq_no = seqNo,
            @eff_date = effDate,
        };
        var r =await _sinoAs01Db.ExecuteAsync(sql,obj);

        return (r > 0) ? new ExecutedSqlJson { PreparedSql = sql, Object = obj } : null; ;
    }
}