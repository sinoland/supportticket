﻿using Api.Models.Database;
using Api.Models.Database.Backup;

namespace Api.Repositories;

public interface IBaseRepository
{
    Task<ExecutedSqlJson?> Update(IDao dao);
    Task<ExecutedSqlJson?> Insert(IDao dao);
}
