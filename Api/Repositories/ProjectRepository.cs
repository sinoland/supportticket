﻿using Api.Models.Database;
using System.Data;
using Dapper;
using Api.Repository;

namespace Api.Repositories;

public class ProjectRepository
{
    private readonly IDbConnection _sinoAs01Db;

    public ProjectRepository(DbResolver db)
    {
        _sinoAs01Db = db(Model.EnumSinoDB.Asdb01);
    }

    public async Task<IEnumerable<ProjectDao>> GetProjectDao(string longNameEn)
    {
        return await _sinoAs01Db.QueryAsync<ProjectDao>($"SELECT * FROM project WHERE long_name_en like @longNameEn", new
        {
            @longNameEn = "%"+longNameEn+"%",
        });
    }
}
