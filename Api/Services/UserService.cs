using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Api.Helper;
using Api.Model.Database;
using Api.Model.Request;
using Api.Repository;
using Microsoft.IdentityModel.Tokens;
using Api.Entities;
using Api.Models;

namespace Api.Service;

public interface IUserService
{
    Task<AuthenticateResponse> Authenticate(AuthenticateRequest model);
    User GetById(int id);
    Task<bool> Register(RegisterRequest request);
}

public class UserService : IUserService
{
    private readonly IConfiguration _configuration;
    private readonly UserRepository _userRepository;

    public UserService(IConfiguration configuration, UserRepository userRepository)
    {
        _configuration = configuration;
        _userRepository = userRepository;
    }

    public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest model)
    {
        var userDao = await _userRepository.SelectUser(model.Username, EncryptionHelper.CreateMD5(model.Password), true);

        // return null if user not found
        if (userDao == null) return null;

        // authentication successful so generate jwt token
        var token = GenerateJwtToken(userDao);

        return new AuthenticateResponse(new User()
        {
            Username = userDao.Username,
            Id = userDao.Id,
        }, token);
    }

    public User GetById(int id)
    {
        // return _users.FirstOrDefault(x => x.Id == id);
        return null;
    }

    public async Task<bool> IsUserExisted(string username, string password)
    {
        var res = await _userRepository.SelectUser(username, EncryptionHelper.CreateMD5(password));
        return res != null;
    }

    public async Task<bool> Register(RegisterRequest request)
    {
        if (await IsUserExisted(request.Username, request.Password)) return false;

        var userDao = new UserDAO
        {
            Username = request.Username,
            Password = EncryptionHelper.CreateMD5(request.Password),
            IsActive = true,
            IsAdmin = false,
            CreateTime = DateTime.Now,
            ModifyTime = DateTime.Now,
        };
        var result = await _userRepository.AddUser(userDao);

        return (result != null);
    }

    // helper methods

    private string GenerateJwtToken(UserDAO user)
    {
        // generate token that is valid for 7 days
        var tokenHandler = new JwtSecurityTokenHandler();

        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtAuth:Key"]));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        var claims = new[]
        {
            new Claim(JwtRegisteredClaimNames.Sub, user.Username),
            new Claim("roles", "Admin"),
            new Claim("Date", DateTime.Now.ToString()),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            // new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
        };
        var token = new JwtSecurityToken(_configuration["JwtAuth:Issuer"],
            _configuration["JwtAuth:Issuer"],
            claims, //null original value
            expires: DateTime.Now.AddMinutes(120),
            signingCredentials: credentials);

        return tokenHandler.WriteToken(token);
    }
}
