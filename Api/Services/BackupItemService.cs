﻿using System.Data;
using System.Text.Json;
using System.Text.Json.Serialization;
using Api.Helper;
using Api.Model;
using Api.Model.Database;
using Api.Models.Database.Backup;
using Api.Repository;
using Dapper;

namespace Api.Service;

public interface IBackupItemService
{
    Task Backup<T>(T obj, Guid tableDataIdentity, string table, T updatedFields,
        EnumSqlConditionActionType actionType);
    Task StoreExecutedSql(int backupItemId, IEnumerable<ExecutedSqlJson> json);
    Task<BackupItemDao> GetOne(Guid tableDataIdentity);
}

public class BackupItemService : IBackupItemService
{
    private readonly BackupItemRepository _backupItemRepository;

    private readonly JsonSerializerOptions options = new JsonSerializerOptions { IncludeFields = true, DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault };

    public BackupItemService(BackupItemRepository backupItemRepository)
    {
        _backupItemRepository = backupItemRepository;
    }

    /**
     * whereCondition no WHERE prefix
     */
    public async Task Backup<T>(T obj, Guid tableDataIdentity, string table, T updatedFields,
        EnumSqlConditionActionType actionType)
    {
        var o = new OriginalDataJson<T>();
        o.Content = obj;
        o.Table = table;
        o.NewDataActionType = actionType;
        o.UpdatedFields = updatedFields;
        await _backupItemRepository.Backup(new BackupItemDao()
        {
            ClassName = typeof(T).FullName,
            OriginalData = JsonSerializer.Serialize(o, options),
            CreateTime = DateTime.Now,
            BackupBeforeEditedByUser = 2,
            TableDataIdentity = tableDataIdentity.ToString(),
            EnumCaseType = EnumCaseType.ArTenancyIncomeInformationEffEndDate,
        });
    }

    // RESTORE
    // public async Task GetData(Guid tableDataIdentity)
    // {
    //     var r = await _backupItemRepository.Get(tableDataIdentity);
    //     var b = JsonSerializer.Deserialize<OriginalDataJson<IBackupItem>>(r.ElementAt(0).OriginalData, options);
    //     var a= r.ToDictionary(k => k.ClassName, v => BackupItemHelper.OriginalDataToObject(v.OriginalData, v.ClassName));
    //     return;
    // }

    public async Task StoreExecutedSql(int backupItemId, IEnumerable<ExecutedSqlJson> json)
    {
        var j = JsonSerializer.Serialize(json, options);
        await _backupItemRepository.AddSql(backupItemId, j);
    }

    public async Task<BackupItemDao> GetOne(Guid tableDataIdentity)
    {
        var r = await _backupItemRepository.Get(tableDataIdentity);
        
        return r.FirstOrDefault();
    }
}
