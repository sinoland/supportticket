@ECHO OFF
ECHO STARTING
setx ASPNETCORE_ENVIRONMENT UAT
start "" ".\Api.exe"
timeout /t 2
REG delete "HKCU\Environment" /F /V "ASPNETCORE_ENVIRONMENT"
pause
