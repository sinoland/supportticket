﻿using System.Text.Json;
using Api.Model.Database;
using Api.Model;
using Api.Models.Request;
using log4net;
using log4net.Repository;
using log4net.Config;

namespace Api.Helper;

public static class LogHelper
{
    private static readonly ILog logInfo = LogManager.GetLogger("NETCoreRepository", ".NET Core Log4net");
    public static void WriteLog(LogRequest request)
    {
        int level = request.Level != null ? (int)request.Level : 0;
        string message = request.Message;

        WriteLog(level, message);
    }
    public static void WriteLog(int level,string message)
    {
        if (level == (int)Log4NetLevel.INFO)
        {
            logInfo.Info(message);
        }
        else if (level == (int)Log4NetLevel.DEBUG)
        {
            logInfo.Debug(message);
        }
        else if (level == (int)Log4NetLevel.WARN)
        {
            logInfo.Warn(message);
        }
        else if (level == (int)Log4NetLevel.ERROR)
        {
            logInfo.Error(message);
        }
        else if (level == (int)Log4NetLevel.FATAL)
        {
            logInfo.Fatal(message);
        }
    }
}