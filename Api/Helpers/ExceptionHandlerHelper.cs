﻿using Api.Models.Request;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Api.Helper;

public class ExceptionHandlerHelper
{
    private readonly RequestDelegate next;

    public ExceptionHandlerHelper(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await next(context);
        }
        catch (Exception ex)
        {
            await HandleExceptionAsync(context, ex);
        }
    }

    private static async Task HandleExceptionAsync(HttpContext context, Exception exception)
    {
        if (exception == null) return;
        await WriteExceptionAsync(context, exception).ConfigureAwait(false);
    }

    private static async Task WriteExceptionAsync(HttpContext context, Exception exception)
    {
        log4net.LogManager.GetLogger("LogInfo").ErrorFormat(string.Format("There is an exception from global and the meessage is,\n{0}\n StackTrace:\n {1}", exception.Message, exception.StackTrace));

        var response = context.Response;

        if (exception is UnauthorizedAccessException)
            response.StatusCode = (int)HttpStatusCode.Unauthorized;
        else if (exception is Exception)
            response.StatusCode = (int)HttpStatusCode.BadRequest;

        response.ContentType = context.Request.Headers["Accept"];

        await response.WriteAsync(exception.Message);
    }
}
